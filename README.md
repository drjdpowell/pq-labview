# libpq.lvclass:libpqpaths.vi
- This method will return a list of paths for your target type (Windows/Linux) and target bitness (32/64 bits)

# libpq32.lvclass and libpq64.lvclass
- These classes function as "containers" to hold libpq.dll. Since libpq.dll has the same name for both 32 and 64 bit targets, these classes are necessary to give each dll its own namespace, otherwise LabVIEW will complain.
- Because the dlls are inside a lvclass, the LabVIEW application builder will recognize the dlls as dependencies and automoatically include them in your build.

# Where to find the libraries?
- libpq.dll is obtained from https://www.postgresql.org/ftp/odbc/releases/
- Dependencies are found and verified using https://github.com/lucasg/Dependencies 
- Any Microsoft Visual C++ Redistributable dependencies should be downloaded and installed from https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170
- pq-labview v0.4.1 ships with libpq.dll v17. If your would like to use a **different** version of libpq.dll for your application simply replace the files in \pq-labview\Connection\lib\win32\ or \pq-labview\Connection\lib\win64\

# How do I install LibPQ on an NI Linux RT target?
- Libraries on Linux are handled a little bit differently, please see: https://forums.ni.com/t5/NI-Linux-Real-Time-Documents/Tutorial-PostgreSQL-with-LabVIEW/tac-p/4111331
